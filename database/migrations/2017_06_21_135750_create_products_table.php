<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->string('description');
            $table->string('location');
            $table->integer('category_id')->unsigned();
            $table->integer('person_id')->unsigned();
           // $table->integer('image_id')->unsigned();          removed to create table images
            $table->integer('status'); //  0 suspended/ 1 sold / 2 active
            $table->integer('priority'); //  0 non /1 high
            $table->string('until'); // run until a specific date
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('person_id')->references('id')->on('people');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
