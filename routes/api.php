<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('postProduct','ApiProductController@postProducts');
//get products
Route::get('getProducts','ApiProductController@getProducts');

//post event
Route::post('postEvent','ApiEventController@postEvent');

// get events getEvents
Route::get('getEvents','ApiEventController@getEvents');