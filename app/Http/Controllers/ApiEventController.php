<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class ApiEventController extends Controller
{
    public function postEvent(Request $request){
        $name=$request->input('name');
        $price=$request->input('price');
        $description=$request->input('description');
        $location=$request->input('location');
        $person_id=$request->input('person_id');
        $duration=$request->input('duration');
        $from=$request->input('from');
        $image=$request->input('image');

        $lastId=DB::table('events')
            ->orderby('id','desc')
            ->first();
        $id=$lastId->id;
        //  $id=0;

        $idd=$id+1;

        $imageName="event$idd.PNG";

        $base64 = base64_decode($image);
        Storage::disk('events')->put($imageName, $base64);

        $today = Carbon::today();
        $until=$today->addDays($duration);
        $untilDate= $until->toDateString();


        $newEvent=new Event();
        $newEvent->name=$name;
        $newEvent->price=$price;
        $newEvent->description=$description;
        $newEvent->location=$location;
        $newEvent->from_date=$from;
        $newEvent->until_date=$untilDate;
        $newEvent->status=2;
        $newEvent->save();

        $event_id=$newEvent->id; //get id of the product to set their images

        $setImage=new EventImage();
        $setImage->image_name=$imageName;
        $setImage->event_id=$event_id;
        $setImage->save();

        return Response::json($setImage);
    }

    public function getEvents(Request $request){

        $today = Carbon::today();
        $current= $today->toDateString();

        $products=DB::table('events')
            ->where('until_date','>=',$current)
            ->join('event_images','event_images.event_id','=','events.id')
            ->orderby('events.id','desc')
            //  ->orderby('id','desc')
            ->get();

        return Response::json($products);
    }
}
