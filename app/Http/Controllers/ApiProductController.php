<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class ApiProductController extends Controller
{

    //post advert product
public function postProducts(Request $request){
    $name=$request->input('name');
    $price=$request->input('price');
    $description=$request->input('description');
    $location=$request->input('location');
    $person_id=$request->input('person_id');
    $category_id=$request->input('category_id');
    $duration=$request->input('duration');
    $image=$request->input('image');

    $lastId=DB::table('products')
        ->orderby('id','desc')
        ->first();
    $id=$lastId->id;
    //  $id=0;

    $idd=$id+1;

    $imageName="product$idd.PNG";

    $base64 = base64_decode($image);
    Storage::disk('products')->put($imageName, $base64);

    $today = Carbon::today();
    $until=$today->addDays($duration);
    $untilDate= $until->toDateString();

    $newProduct=new Product();
    $newProduct->name=$name;
    $newProduct->price=$price;
    $newProduct->description=$description;
    $newProduct->location=$location;
    $newProduct->person_id=$person_id;
    $newProduct->category_id=$category_id;
    $newProduct->until=$untilDate;
    $newProduct->status=2;
    $newProduct->priority=1;
    $newProduct->save();

    $product_id=$newProduct->id; //get id of the product to set their images

    $setImage=new Image();
    $setImage->image_name=$imageName;
    $setImage->product_id=$product_id;
    $setImage->save();

    return Response::json($setImage);

}

public function getProducts(Request $request){
    $today = Carbon::today();
    $current= $today->toDateString();

    $products=DB::table('products')
        ->where('until','>=',$current)
        ->join('images','images.product_id','=','products.id')
        ->orderby('priority','desc')
         ->orderby('products.id','desc')
        ->get();

    return Response::json($products);
}

}
